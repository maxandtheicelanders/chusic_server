var db = require('../config/mongo_database.js');
var express = require('express');
var router = express.Router();

router.get('/create', function(req, res) {
  res.render('reg');
});


router.get('/play/:id', function(req, res) {
  console.log("inside get event");
  console.log(req.body);
  db.eventModel.findOne({_id: req.params.id},function(err, results) {
    if (err) {
      console.log(err);
      return res.send(400);
    }
    console.log("yeah");
    res.render('events',results);

  
  });
});


// takes the info about a event, name, description and each song
router.post('/', function(req, res) {
  var eventEntry ={};
  console.log(req.body);
  eventEntry.name = req.body.name;
  eventEntry.description = req.body.description;
  eventEntry.playlist=[
    {'artist':'The Proclaimers', 'title':'I\'m gonna be (500 miles)', 'keyid':'p11544918','score':0},
    {'artist':'U2', 'title':'Vertigo', 'keyid':'p11544893','score':0},
    {'artist':'Blink-182', 'title':'All the small things', 'keyid':'p11544873','score':0},
    {'artist':'Jimi Hendrix', 'title':'Purple Haze', 'keyid':'p11544865','score':0},
    {'artist':'Journey', 'title':'Don\'t stop believing', 'keyid':'p11544830','score':0},
    {'artist':'Roxy Music', 'title':'Hurricane', 'keyid':'p11544815','score':0},
    {'artist':'Beach Boys', 'title':'God only knows', 'keyid':'p11544809','score':0},
    {'artist':'Sigma', 'title':'Nobody to love', 'keyid':'p11544795','score':0},
    {'artist':'Toto', 'title':'Africa', 'keyid':'p11544788','score':0}
  ];
  
  db.eventModel.create(eventEntry,function(err,data2) {
    if (err) {
      console.log(err);
      return res.send(400);
    }
    
    return res.redirect('/events/play/' + data2._id);
  });

});

router.post('/downvote', function(req, res) {
  var id_event = req.body.id;
  var id_song = req.body.id;
  db.eventModel.update({_id:id_event, "playlist.keyid":id_song} , {$inc:{"playlist.$.score":-1}} , function(err, nbRows, raw) {
   if (err) {
    console.log(err);
    return res.send(400);
   }
    return res.send(200,raw);
  });
});

router.post('/upvote', function(req, res) {
  var id_event = req.body.id;
  var id_song = req.body.id;
  db.eventModel.update({_id:id_event, "playlist.keyid":id_song} , {$inc:{"playlist.$.score":+1}} , function(err, nbRows, raw) {
   if (err) {
    console.log(err);
    return res.send(400);
   }
    return res.send(200,raw);
  });
});

module.exports = router;
