var db = require('../config/mongo_database.js');


module.exports = function(socket){
  socket.on('upvote', function(songInfo){

  	var id_event = songInfo.event_id;
  	var id_song = songInfo.song_id;
  	db.eventModel.update({_id:id_event, "playlist.keyid":id_song} , {$inc:{"playlist.$.score":+1}} , function(err, nbRows, raw) {
  	 if (err) {
  	  console.log(err);
  	  return res.send(400);
  	 }
  	  return res.send(200,raw);
  	});


    
  });

  socket.on('downvote', function(songInfo){

  	var id_event = songInfo.event_id;
  	var id_song = songInfo.song_id;
  	db.eventModel.update({_id:id_event, "playlist.keyid":id_song} , {$inc:{"playlist.$.score":-1}} , function(err, nbRows, raw) {
  	 if (err) {
  	  console.log(err);
  	  return res.send(400);
  	 }
  	  return res.send(200,raw);
  	});

  });

  socket.on('nextSong', function(playListInfo){

  	db.eventModel.update({_id:playListInfo.event_id} , playListInfo.playlist , function(err, nbRows, raw) {
  	 if (err) {
  	  console.log(err);
  	 }
  	 socket.emit('plUpdate', raw);
  	});
  });

}