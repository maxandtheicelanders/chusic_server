var doneVoting = false;

angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {
  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('PlaylistViewCtrl', ['$scope', 'Playlist', function($scope, Playlist) {
  // $scope.playlist = Playlist.returnPlaylist();
  $scope.search = '';

  $scope.playlist = Playlist.returnPlaylist();

  $scope.voteDown = function(songID){


    console.log('jquery kjellinn')
    var idxFind = $scope.findSongID(songID);
    if ( !$scope.playlist.songs[idxFind].hasVoted){
      $.ajax({
        type: 'POST',
        url: "http://146.185.145.86:3000/events/downvote",
        crossDomain: true, // enable this
        dataType: 'json',
        data: { songID:idxFind}
      });
      $scope.playlist.songs[idxFind].votes--;
      $scope.playlist.songs[idxFind].hasVoted = true;
    };
  };

  $scope.voteUp = function(songID){
    var idxFind = $scope.findSongID(songID);

    // $.post("http://146.185.145.86:3000/upvote", function( data ){
    //   alert('Nadum ad senda, upp, upp, min sal!')
    // });

    if ( !$scope.playlist.songs[idxFind].hasVoted){
      $scope.playlist.songs[idxFind].votes++;
      $.ajax({
        url: "http://146.185.145.86:3000/events/upvote",
        crossDomain: true, // enable this
        dataType: 'json',
        data: { songID:idxFind},
        type: 'POST'
      })
      $scope.playlist.songs[idxFind].hasVoted = true;
    }
  };

  $scope.findSongID = function(songID){
      for (var songCounter = 0; songCounter < $scope.playlist.songs.length; songCounter++) {
        if ($scope.playlist.songs[songCounter].id === songID){
          return idx = songCounter;
        }
    }  
  }

}])

.controller('StartPageCtrl', function($scope, $ionicPopup, $state) {
    $scope.loginIcon = "ion-log-in";
    $scope.eventID = "";
    var myPopup;

    $scope.showPopup = function() {
    $scope.data = {}

    // An elaborate, custom popup
    myPopup = $ionicPopup.show({
      templateUrl:'templates/popup-joinParty.html',
      scope: $scope,
      buttons: [
        // { text: 'Cancel' },
        // {
        //   icon: 'ion-friend-add',
        //   type: 'button-positive',
        //   onTap: function(e) {
        //     if (!$scope.data.wifi) {
        //       //don't allow the user to close unless he enters wifi password
        //       e.preventDefault();
        //     } else {
        //       return $scope.data.wifi;
        //     }
        //   }
        // },
      ]
    });
    myPopup.then(function(res) {
      console.log('Tapped!', res);
    });
  };

  $scope.cancelJoining = function() {
    console.log('Closing in controller!');
    myPopup.close();

  }

  $scope.joinParty = function() {
    $scope.loginIcon = "ion-loading-a";
    console.log('Joining a party');

    $.ajax({
        url: "http://146.185.145.86:3000/events/",
        crossDomain: true, // enable this
        dataType: 'json',
        data: { name: "HackZurich"},
        success: function(data){console.log('Baragaman')},
        type: 'GET'
      })

    setTimeout(function(){
      $state.go('app.playlistView');
       myPopup.close();},
      2500);

  }

})


.controller('PlaylistCtrl', function($scope, $stateParams) {
});
