var baseUrl = 'http://localhost:3000/';

angular.module('starter.services', [])

.factory('Playlist', function() {
  // Might use a resource here that returns a JSON array

  var playlist = {name:"DJ Ötzi @ ETH", songs:
    [{ artist:'Toto', title: 'Africa', id: 1, votes:12, hasVoted: false },
    { artist:'Psy', title: 'Gangnam Style', id: 2, votes:10, hasVoted: false },
    { artist:'Rebecca Black', title: 'Friday', id: 3, votes:5, hasVoted: false },
    { artist:'Tay Zonday', title: 'Chocolate Rain', id: 4, votes:6, hasVoted: false },
    { artist:'Rick Astley', title: 'Never Gonna Give You Up', id: 5, votes:10, hasVoted: false },
    { artist:'Duran Duran', title: 'View to a Kill', id: 6, votes:3, hasVoted: false },
    { artist:'Justin Bieber', title: 'Baby', id: 7, votes:-3, hasVoted: false },
    { artist:'David Hasselhoff', title: 'Hooked on a Feeling', id: 8, votes:4, hasVoted: false }
  ]};


  return {
    returnPlaylist: function(){
      return playlist;
    },
  }
})

.factory('socket', function socket($rootScope) {
  var socket = io.connect(baseUrl);
  return {
    on: function (eventName, callback) {
      socket.on(eventName, function () {  
        var args = arguments;
        $rootScope.$apply(function () {
          callback.apply(socket, args);
        });
      });
    },
    emit: function (eventName, data, callback) {
      socket.emit(eventName, data, function () {
        var args = arguments;
        $rootScope.$apply(function () {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      })
    }
  };
})