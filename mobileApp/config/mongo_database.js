var mongoose = require('mongoose');
var mongodbURL = 'mongodb://localhost/test';
var mongodbOptions = {}

mongoose.connect(mongodbURL, mongodbOptions, function (err, res) {
  if (err) {
    console.log('Connection refused to ' + mongodbURL);
    console.log(err);
  } else {
    console.log('Connection successful to: ' + mongodbURL);
  }
});

var Schema = mongoose.Schema;


var Event = new Schema({
    name: { type: String, required: true },
    descript: { type: String },
    created: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now },
    playlist_id: { type: Number },
});


var Playlist = new Schema({
    name: { type: String, required: true },
    event_id: { type: Number },
    descript: { type: String },
    created: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now },
    event_id: { type: Number  },
    songs: [{ spotify_url: String, name: String, artist: String, votes: Number}],
});





//Define Models
var eventModel = mongoose.model('Event', Event);
var playlistModel = mongoose.model('Playlist', Playlist);


// Export Models
exports.eventModel = eventModel
exports.playlistModel = playlistModel

