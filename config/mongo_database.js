var mongoose = require('mongoose');
var mongodbURL = 'mongodb://127.0.0.1/test';
var mongodbOptions = {}

mongoose.connect(mongodbURL, mongodbOptions, function (err, res) {
  if (err) {
    console.log('Connection refused to ' + mongodbURL);
    console.log(err);
  } else {
    console.log('Connection successful to: ' + mongodbURL);
  }
});

var Schema = mongoose.Schema;


var Event = new Schema({
    name: { type: String, required: true },
    description: { type: String },
    created: { type: Date, default: Date.now },
    playlist: [{ keyid: String, title: String, artist: String, score: Number}]
});






//Define Models
var eventModel = mongoose.model('Event', Event);


// Export Models
exports.eventModel = eventModel

