/*
Copyright (c) 2011 Rdio Inc

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 */

// a global variable that will hold a reference to the api swf once it has loaded
var apiswf = null;
var playlists = null;

var clicked = false;

var songlist = [
  {'id':0, 'artist':'The Proclaimers', 'title':'I\'m gonna be (500 miles)', 'keyid':'p11544918','score':1},
  {'id':1, 'artist':'U2', 'title':'Vertigo', 'keyid':'p11544893','score':10},
  {'id':2, 'artist':'Blink-182', 'title':'All the small things', 'keyid':'p11544873','score':6},
  {'id':3, 'artist':'Jimi Hendrix', 'title':'Purple Haze', 'keyid':'p11544865','score':3},
  {'id':4, 'artist':'Journey', 'title':'Don\'t stop believing', 'keyid':'p11544830','score':20},
  {'id':5, 'artist':'Roxy Music', 'title':'Hurricane', 'keyid':'p11544815','score':14},
  {'id':6, 'artist':'Beach Boys', 'title':'God only knows', 'keyid':'p11544809','score':12},
  {'id':7, 'artist':'Sigma', 'title':'Nobody to love', 'keyid':'p11544795','score':1},
  {'id':8, 'artist':'Toto', 'title':'Africa', 'keyid':'p11544788','score':18}

];

var currentsong = 7;



oauthresult = null;


// //Initialise OAuth.js
// OAuth.initialize('DzbhFBXT2C9PUQeV31klofwBeT4');

// OAuth.popup('facebook')
//     .done(function(result) {
//       //use result.access_token in your API request 
//       //or use result.get|post|put|del|patch|me methods (see below)

//       oauthresult = result;
//       console.log(result);

//       result.post('http://api.rdio.com/1/',{'data': {'method':'getPlaylists'}})
//       .done(function (response){
//         console.log('Call succeeded!!!')
//         console.log(response)
//       })
//       .fail(function(err){
//         console.log('Call failed!!!')
//         console.log(err)

//       });

//       // result.me()
//       // .done(function(result){
//       //   console.log(result)
//       // })
//       // .fail(function(err){

//       // });

//       //result.acces_token

//     })
//     .fail(function (err) {
//       //handle error with err
// });





//Make jquery GET call to get playlists from rdio

// $.post('http://api.rdio.com/1',{body:{'method':'getPlaylists'}},function(data){
//   console.log(data);
// });


//Set up SWF object to take care of playing music

$(document).ready(function() {
  // on page load use SWFObject to load the API swf into div#apiswf
  var flashvars = {
    'playbackToken': playback_token, // from token.js
    'domain': domain,                // from token.js
    'listener': 'callback_object'    // the global name of the object that will receive callbacks from the SWF
    };
  var params = {
    'allowScriptAccess': 'always'
  };
  var attributes = {};
  swfobject.embedSWF('http://www.rdio.com/api/swf/', // the location of the Rdio Playback API SWF
      'apiswf', // the ID of the element that will be replaced with the SWF
      1, 1, '9.0.0', 'expressInstall.swf', flashvars, params, attributes);


  // set up the controls
  $('#play').click(function() {
    clicked = true;
    apiswf.rdio_play(songlist[currentsong].keyid);
    setTimeout(function(){
      clicked = false;
      console.log('clicked is now '+clicked)
    },500);
  });
  $('#stop').click(function() { 
    clicked = true;
    apiswf.rdio_stop(); 
    setTimeout(function(){
      clicked = false;
    },500);
  });
  $('#pause').click(function() { apiswf.rdio_pause(); });
  $('#previous').click(function() { apiswf.rdio_previous(); });
  $('#next').click(function() { 
    clicked = true;
    apiswf.rdio_stop(); 
    currentsong = currentsong +1; 
    apiswf.rdio_play(songlist[currentsong].keyid); 
    setTimeout(function(){
      clicked = false;
    },500);
  });
});


// the global callback object
var callback_object = {};

callback_object.ready = function ready(user) {
  // Called once the API SWF has loaded and is ready to accept method calls.

  // find the embed/object element
  apiswf = $('#apiswf').get(0);

  apiswf.rdio_startFrequencyAnalyzer({
    frequencies: '10-band',
    period: 100
  });

  if (user == null) {
    $('#nobody').show();
  } else if (user.isSubscriber) {
    $('#subscriber').show();
  } else if (user.isTrial) {
    $('#trial').show();
  } else if (user.isFree) {
    $('#remaining').text(user.freeRemaining);
    $('#free').show();
  } else {
    $('#nobody').show();
  }

  console.log(user);
}

callback_object.freeRemainingChanged = function freeRemainingChanged(remaining) {
  $('#remaining').text(remaining);
}

callback_object.playStateChanged = function playStateChanged(playState) {
  // The playback state has changed.
  // The state can be: 0 - paused, 1 - playing, 2 - stopped, 3 - buffering or 4 - paused.
  $('#playState').text(playState);

  // setTimeout(function(){
  
  // if(playState===2){
  //   console.log('The playstate is '+playState+' so we play next song')
  //   currentsong = currentsong + 1;
  //   apiswf.rdio_play(songlist[currentsong].keyid);
  // }
  // },3000);
}

callback_object.playingTrackChanged = function playingTrackChanged(playingTrack, sourcePosition) {
  // The currently playing track has changed.
  // Track metadata is provided as playingTrack and the position within the playing source as sourcePosition.
  console.log(playingTrack)

  if (playingTrack != null) {
    $('#track').text(playingTrack['name']);
    $('#album').text(playingTrack['album']);
    $('#artist').text(playingTrack['artist']);
    $('#art').attr('src', playingTrack['icon']);
  }
  else if(!clicked){
    currentsong = currentsong +1; 
    apiswf.rdio_play(songlist[currentsong].keyid);
    clicked = true;
    setTimeout(function(){
      clicked = false;
    },500);
  }
}

callback_object.playingSourceChanged = function playingSourceChanged(playingSource) {
  // The currently playing source changed.
  // The source metadata, including a track listing is inside playingSource.
}

callback_object.volumeChanged = function volumeChanged(volume) {
  // The volume changed to volume, a number between 0 and 1.
}

callback_object.muteChanged = function muteChanged(mute) {
  // Mute was changed. mute will either be true (for muting enabled) or false (for muting disabled).
}

callback_object.positionChanged = function positionChanged(position) {
  //The position within the track changed to position seconds.
  // This happens both in response to a seek and during playback.
  $('#position').text(position);
}

callback_object.queueChanged = function queueChanged(newQueue) {
  // The queue has changed to newQueue.
}

callback_object.shuffleChanged = function shuffleChanged(shuffle) {
  // The shuffle mode has changed.
  // shuffle is a boolean, true for shuffle, false for normal playback order.
}

callback_object.repeatChanged = function repeatChanged(repeatMode) {
  // The repeat mode change.
  // repeatMode will be one of: 0: no-repeat, 1: track-repeat or 2: whole-source-repeat.
}

callback_object.playingSomewhereElse = function playingSomewhereElse() {
  // An Rdio user can only play from one location at a time.
  // If playback begins somewhere else then playback will stop and this callback will be called.
}

callback_object.updateFrequencyData = function updateFrequencyData(arrayAsString) {
  // Called with frequency information after apiswf.rdio_startFrequencyAnalyzer(options) is called.
  // arrayAsString is a list of comma separated floats.

  var arr = arrayAsString.split(',');

  $('#freq div').each(function(i) {
    $(this).width(parseInt(parseFloat(arr[i])*500));
  })
}

